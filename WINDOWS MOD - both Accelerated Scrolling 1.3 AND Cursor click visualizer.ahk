

; Accelerated Scrolling
; V1.3
; By BoffinbraiN
; https://www.youtube.com/watch?v=OobKVPojFmg
; ORIGINAL:  https://autohotkey.com/board/topic/48426-accelerated-scrolling-script/

/*
PLUS cursor click visualization 1.0, By Taran Van Hemert.
PROBLEMS WITH THE CURSOR VISUALIZER:
Background color behind the green text is not completely transparent on the edges --- there is no anti aliasing, so it looks jagged
Would be nice to use customizable pictures, instead of a dumb font (lucida console) that not everyone will have anyway.
The click visualizations do not linger at all -- I don't know how to do this if I wanted to.
Middle click visualization does not work. I don't know why but nothing I try will work to get it to show up.
Also, You'll have to modify the positioning of the GUI to suit your own computer and its UI scaling in Windows.
*/

;Use this script only if you want cursor visualization, AND accelrated scrolling.
;SOURCE: https://github.com/TaranVH/2nd-keyboard

Menu, Tray, Icon, shell32.dll, 44 ; changes the icon to a star -- makes it easy to see which one it is.


SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#NoEnv
;#NoTrayIcon
#SingleInstance
#MaxHotkeysPerInterval 2000
Process, Priority, , H
SendMode Input
#SingleInstance force
CoordMode, mouse, screen

;below is a bunch of GUI stuff that I need for cursor click visualization.
;Use WIN KEY and scroll down to turn it on or off.

size := 200
kolor := 11FF99
kolor2 := 008833
showClicks = 1






Gui, -Caption +ToolWindow +AlwaysOnTop +LastFound
;Gui, Color, %kolor2%
Gui, Color, 008833
GuiHwnd := WinExist()
DetectHiddenWindows, On
WinSet, Region, % "0-0 W" Size " H" Size, ahk_id %GuiHwnd%
WinSet, ExStyle, +0x20, ahk_id %GuiHwnd% ; set click through style
;WinSet, Transparent, 0, ahk_id %GuiHwnd% ;didn't work...
;WinSet, TransColor, %kolor2% 100
WinSet, TransColor, 008833 100
;WinSet, TransColor, %kolor% 100
Gui, Show, w%Size% h%Size% ;hide
Gui, Font, s32, lucida console ;This font has nearly perfect ()v^ characters. I also tried these: ;Estrangelo Edessa ;century gothic
Gui, Add, Text, vMyText cLime,  ; xx  auto-sizes the window.

If showClicks = 1
{
	settimer, looper, -2
}
nokill = 1
delay = 200





; pgup::send {wheelup 50}
; pgdn::send {wheeldown 50}




;Begin cursor click visualization stuff:

~*$LButton::
Lbuttondown = 1
Return
~*$LButton up::
Lbuttondown = -1
Return

~*$RButton::
Rbuttondown = 1
Return
~*$RButton up::
Rbuttondown = -1
Return


wheelkiller:
;msgbox, wheelkiller
if Wup = -1
{
Wup = 0
Gui, hide
}
if Wdown = -1
{
Wdown = 0
Gui, hide
}
Return


looper:
MouseGetPos, realposX, realposY
;posX := realposX - Size/2.4, posY := realposY - Size/3.1 + 0 ; puts the gui in the middle rather than the corner. You'll have to change these variables if you have UI scaling set to 100% rather than 150% like I do.
posX := realposX - Size/3.5, posY := realposY - Size/4.5 + 0 ; puts the gui in the middle rather than the corner. You'll have to change these variables if you have UI scaling set to 100% rather than 150% like I do.

If(Lbuttondown = 1 and Rbuttondown = 1) ;this doesn't really work. Often the ) will be out of alignment or will not show up.
    {
		GuiControl,, MyText, ()
		posX := posX-10
		Gui, Show, x%posX% y%posY% NA
		settimer, looper, -20
		return
	}

If Lbuttondown = 1
{
	;tooltip, <, posX-25, posY-8, 2 ;this code was used for bug testing.
	;SoundBeep, 400, 50
	;soundplay, C:\AHK\3.wav,
	GuiControl,, MyText, (
	posXX := posX-10
	Gui, Show, x%posXX% y%posY% NA
}
If Lbuttondown = -1
{
	;tooltip, , , , 2
	Lbuttondown = 0
	Gui, hide
}


If Rbuttondown = 1
{
	posX := posX+20
	GuiControl,, MyText, )
	Gui, Show, x%posX% y%posY% NA
}
If Rbuttondown = -1
{
	Rbuttondown = 0
	Gui, hide
}

If Wup = 1
{
	posYu := posY-20
	posxu := posX+8
	GuiControl,, MyText, ^
	Gui, Show, x%posXu% y%posYu% NA
	settimer, wheelkiller, -200
	Wup = -1
}
If Wdown = 1
{
	posYd := posY+20
	posXd := posX+8
	Gui, Font, s20 cBlue, Verdana
	GuiControl,, mytext, v
	Gui, Show, x%posXd% y%posYd% NA
	
	
	settimer, wheelkiller, -200
	Wdown = -1
}


settimer, looper, -1
Return



